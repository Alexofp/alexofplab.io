$(document).ready(function(){

	$('#fullpage').fullpage({
		navigation: false,
		responsiveWidth: 0,
		scrollOverflow: true,
		
		
		anchors: ['news' ,'main', 'about', 'gallery', 'info'],
		//parallax: true,
		onLeave: function(origin, destination, direction){
			if(origin.anchor == "news") {
				$( ".news__categories" ).addClass( "news__categories-hide" );
			}
			if(destination.anchor == "news") {
				$( ".news__categories" ).removeClass( "news__categories-hide" );
			}
			
			if(destination.anchor == "main") {
				$( ".navbutton_main" ).addClass( "main__menu__button-hide" );
			}else{
				$( ".navbutton_main" ).removeClass( "main__menu__button-hide" );
			}
			if(destination.anchor == "about") {
				$( ".navbutton_info" ).addClass( "main__menu__button-hide" );
			}else{
				$( ".navbutton_info" ).removeClass( "main__menu__button-hide" );
			}
			if(destination.anchor == "gallery") {
				$( ".navbutton_gallery" ).addClass( "main__menu__button-hide" );
			}else{
				$( ".navbutton_gallery" ).removeClass( "main__menu__button-hide" );
			}
			
			if(['news' ,'main'].indexOf(destination.anchor) !== -1) {
				$( ".main__menu" ).removeClass( "main__menu-hide" );
				$( ".main__menu__burger" ).removeClass( "main__menu__burger-out" );
				
				$('.main__menu__burger').removeClass( "main__menu__burger-down" );
				$('.main__menu').removeClass( "main__menu-smaller" );
			}
			if(['about', 'gallery', 'info'].indexOf(destination.anchor) !== -1 ) {
				$( ".main__menu" ).addClass( "main__menu-hide" );
				$( ".main__menu__burger" ).addClass( "main__menu__burger-out" );
			}

		},
		afterRender: function () {
			if(!window.location.hash) {
				$.fn.fullpage.silentMoveTo(2,1);
			}

			setTimeout(function(){
				$('.news__categories').addClass( "news__categories-smooth" );
				$('.main__menu').addClass( "main__menu-smooth" );
				$('.main__menu__burger').addClass( "main__menu__burger-smooth" );
			}, 10);
			
		}
	});
	
    $('.navbutton_news').click(function(event){
		$.fn.fullpage.moveTo('news');
		
		$(".mobile__menu").removeClass("mobile__menu-active");
    });
	
	$('.navbutton_main').click(function(event){
		$.fn.fullpage.moveTo('main');
		
		$(".mobile__menu").removeClass("mobile__menu-active");
    });
	
	$('.navbutton_info').click(function(event){
		$.fn.fullpage.moveTo('about');
		
		$(".mobile__menu").removeClass("mobile__menu-active");
    });
	
	$('.navbutton_gallery').click(function(event){
		$.fn.fullpage.moveTo('gallery');
		
		$(".mobile__menu").removeClass("mobile__menu-active");
    });
	
	$('.navbutton_share').click(function(event){
		$(".shareoverlay__back").addClass("shareoverlay-active");
		
		$(".mobile__menu").removeClass("mobile__menu-active");
    });
	
	$('.mobile__menu__button').click(function(event){
		$(".mobile__menu").toggleClass("mobile__menu-active");
    });
	
	$('.shareoverlay__back').click(function(event){
		if(event.target != this) return;
		
		$(".shareoverlay__back").removeClass("shareoverlay-active");
    });
	
	$('.shareoverlay .close-btn').click(function(event){
		$(".shareoverlay__back").removeClass("shareoverlay-active");
    });
	
	
	$('.infopage__mainimage__arrow').click(function(event){
		// change to actual shifting
		var pics = ["img/gallery/ART06059.jpg", "img/gallery/Zhang Guolao/0394.jpg", "img/gallery/Zhang Guolao/0394_0.jpg", "img/gallery/Zhang Guolao/0394_1.jpg", "img/gallery/Zhang Guolao/0394_2.jpg", "img/gallery/Zhang Guolao/0394_3.jpg", "img/gallery/Zhang Guolao/0483.jpg"];
		
		// picks randoms
		const randomElement = pics[Math.floor(Math.random() * pics.length)];
		
		$('.infopage__mainimage').attr('src', randomElement);
    });
	
	
	
	$('.infopage__3dmodelbutton').click(function(event){
		event.preventDefault();
		var src = $(this).attr("data-url");
		
		if($("#model").hasClass("iframe3dmodel-visible")) {
			$("#model").removeClass("iframe3dmodel-visible");
			$("#player").attr("src", "");
		}else{
			$("#model").addClass("iframe3dmodel-visible");
			$("#model").attr("src", src);
		}
	});
	
    $('.main__menu__burger-icon').click(function(event){
		$('.main__menu__burger').toggleClass( "main__menu__burger-down" );
		$('.main__menu').toggleClass( "main__menu-smaller" );
    })
	
	// video overlayer: start
	$(".js-overlay-start").click(function(e) {
		e.preventDefault();
		var src = $(this).attr("data-url");
		$(".overlay-video").show();
		setTimeout(function() {
			$(".overlay-video").addClass("o1");
			$("#player").attr("src", src);
		}, 100);
	});
	
	// video overlayer: close it if you click outside of the modal
	$(".overlay-video").click(function(event) {
		if (!$(event.target).closest(".videoWrapperExt").length) {
			var PlayingVideoSrc = $("#player").attr("src").replace("&autoplay=1", "");
			$("#player").attr("src", PlayingVideoSrc);
			$(".overlay-video").removeClass("o1");
			setTimeout(function() {
				$(".overlay-video").hide();
			}, 600);
		}
	});

	// video overlayer: close it via the X icon
	$(".videoclose").click(function(event) {
			var PlayingVideoSrc = $("#player").attr("src").replace("&autoplay=1", "");
			$("#player").attr("src", PlayingVideoSrc);
			$(".overlay-video").removeClass("o1");
			setTimeout(function() {
				$(".overlay-video").hide();
			}, 600);

	});
	
	
	$('.news__categories').on('wheel', function(event) {
	  if (event.originalEvent.deltaY > 0) {
		  var current = $.fn.fullpage.getActiveSection();
		  if(current.anchor == "news") {
			  $.fn.fullpage.moveSectionDown();
		  }
	  }
	});


	$('.carousel1').slick({
		infinite: true,
		draggable: false,
		slidesToShow: 1,

		variableWidth: true
	});
	$('.carousel2').slick({
		infinite: true,
		draggable: false,
		slidesToShow: 1,

		variableWidth: true
	});
	$('.carousel3').slick({
		draggable: false,
		vertical: true,
		adaptiveHeight: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		verticalSwiping: true,
	});
	
	$('.carousel2 .gallery-item, .carousel1 .gallery-item').click(function(event){
		$.fn.fullpage.moveSectionDown();
		// Add info filling code
    });
	
	$('.infopage-back').click(function(event){
		$.fn.fullpage.moveSectionUp();
    });
	
	$('.carousel3 .gallery-item').click(function(event){
		var img = $(this).find(">:first-child").attr('src');
		
		$('.infopage__mainimage').attr('src', img);
		
    });
	
	setTimeout(rotatingthing1, 2000+Math.random()*8000);

	function rotatingthing1( )
	{
		$('.welcomepage__topleft-left .flippingContainer').toggleClass( "flippingContainer-rotated" );
		setTimeout(rotatingthing1, 2000+Math.random()*8000);
	}
	
	setTimeout(rotatingthing2, 2000+Math.random()*8000);

	function rotatingthing2( )
	{
		$('.welcomepage__right-bottom .flippingContainer').toggleClass( "flippingContainer-rotated" );
		setTimeout(rotatingthing2, 2000+Math.random()*8000);
	}
});