$(document).ready(function(){
	$('.main__menu__burger').click(function(event){
		$(".sidemenu").addClass("sidemenu-show");
    });
	
	$('.sidemenu__close').click(function(event){
		$(".sidemenu").removeClass("sidemenu-show");
    });
	
	$('.main__menu__avatar').click(function(event){
		$(".profilepane").toggleClass("profilepane-visible");
    });
	$('.profilepane__cancel').click(function(event){
		$(".profilepane").removeClass("profilepane-visible");
    });
	
	$('.main__menu__loginbutton').click(function(event){
		$(".login-wrapper").addClass("login-visible");
    });
	
	$('.login-wrapper').click(function(event){
		if(event.target != this) return;
		
		$(".login-wrapper").removeClass("login-visible");
    });
	
	
	$('.objext__thumbnails__wrapper').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 3,
	  slidesToScroll: 3,
	  centerMode: false,
	  variableWidth: true
	});
	
	$('.slick__list_6').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  centerMode: false,
	  variableWidth: true
	});
	
	$('.slick__list_4').slick({
	  dots: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  centerMode: false,
	  variableWidth: true
	});
});